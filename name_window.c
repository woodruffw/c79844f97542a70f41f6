/*	name_window
	Name the given window "name" on the given display.

	Arguments:
	Display *disp - a pointer to the X display
	Window wind - the window being named
	const char *name - the name being given to the window
*/
void name_window(Display *disp, Window wind, const char *name)
{
	XTextProperty name_prop;

	name_prop.value = (unsigned char *) name;
	name_prop.encoding = XA_STRING; /* found in X11/Xatom.h */
	name_prop.format = 8; /* our format is 8 bits, ASCII encoded */
	name_prop.nitems = strlen(name); /* specify the number of characters in name */

	XSetWMName(disp, wind, &name_prop);
}
